/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package getmqmessages;
import com.am.mail.Email;
import com.am.mail.vo.Mail;
import com.am.mail.vo.Server;
import com.am.mq.MQ;
import com.am.mq.vo.Header;
import com.am.mq.vo.Message;
import com.am.mq.vo.Queue;
import com.am.sftp.Sftp;
import com.am.sftp.vo.File;
import com.am.sftp.vo.Ftp;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.*; 


/**
 *
 * @author eduardo
 */
public class GetMQMessages {
    
public static final String DATE_FORMAT_NOW = "yyyyMMdd";   
public static final String DATE_FORMAT_TIMESTAMP = "yyyy-MM-dd HH:mm:ss";   
public static final String DATE_FORMAT_NOW_DATETIME = "yyyyMMdd_HH_mm_ss"; 
public static final String DATE_FORMAT_NOW_DATETIME2 = "yyyyMMddHHmmssSSS"; 
public static final Boolean PurgeQueue = false;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Testscenario();
        // testMqPut();
        // GetMessagesFromQueue();
        startAPIGetMessages();

    }
    
    public static void startAPIGetMessages() throws InterruptedException{
        do{
            System.out.println(GetMQMessages.getStringCurrentTimeStamp() +  " Reviewing queue.");
            getAPISMessagesfromAtrium();
            System.out.println("+++++++++++++++++ SLEEPING 1 MIN ++++++++++++++");
            TimeUnit.MINUTES.sleep(1);
        }while (true);
    }
    
    public static Queue getQueueConectionTEST (){
        Queue queue = new Queue();
        queue.setHost("172.18.60.97");
        queue.setPort(1818);
        
        queue.setManager("AMMT");
        queue.setName("JEPP.AMX.APIS1");
        queue.setChannel("AMMT.ADMIN.01");

        return queue;
    }
    
    public static Queue getQueueConectionPROD (){
        Queue queue = new Queue();
        queue.setHost("172.18.60.120");
        queue.setPort(5414);
        
        queue.setManager("EHEB");
        queue.setChannel("DISPATCHM.EHEB.01");
        queue.setName("JEPP.AMX.APIS1");

        return queue;
    }
    
      
       public static Queue getQueueSharesProd(){
        Queue queue = new Queue();
        queue.setHost("172.18.60.120");
        queue.setPort(5000);
        
        queue.setManager("EHEB");
        queue.setChannel("MANTIS.EHEB.CHANNEL");
        queue.setName("A1Y1PSHB.TYPEB.QR");

        return queue;
    }
    
         
    public static void writeTxt(String fileName, String text)
            throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
        writer.append(' ');
        writer.append(text);

        writer.close();
    }
    
    private static void testMqPut() {

        try {
            Queue queue = getQueueConectionTEST();

            //-- Put
            Message msg = new Message();
            msg.setMessage("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><IATA_AIDX_FlightLegNotifRQ xmlns=\"http://www.iata.org/IATA/2007/00\" TimeStamp=\"2019-05-21T18:27:21\" Target=\"Test\" Version=\"1.0\" TransactionIdentifier=\"17802052\" PrimaryLangID=\"en-us\"><Originator CompanyShortName=\"AMX\" TravelSector=\"A\" Code=\"AM\" CodeContext=\"3\"/><FlightLeg><LegIdentifier><Airline>AM</Airline><FlightNumber>668</FlightNumber><DepartureAirport CodeContext=\"3\">MEX</DepartureAirport><ArrivalAirport CodeContext=\"3\">SFO</ArrivalAirport><OriginDate>2019-05-21</OriginDate><RepeatNumber CurrentInd=\"true\">1</RepeatNumber></LegIdentifier><LegData InternationalStatus=\"International\"><OperationTime OperationQualifier=\"TDN\" CodeContext=\"9750\" TimeType=\"EST\">2019-05-21T19:01:01</OperationTime><OperationTime OperationQualifier=\"TDN\" CodeContext=\"9750\" TimeType=\"ESL\">2019-05-21T12:01:01</OperationTime><OperationTime OperationQualifier=\"ONB\" CodeContext=\"9750\" TimeType=\"EST\">2019-05-21T19:06:01</OperationTime><OperationTime OperationQualifier=\"ONB\" CodeContext=\"9750\" TimeType=\"ESL\">2019-05-21T12:06:01</OperationTime></LegData><TPA_Extension/></FlightLeg></IATA_AIDX_FlightLegNotifRQ>");
            msg.setHeader(new Header());
            msg.getHeader().setMsdType("Passur-Hdr-H1");
            msg.getHeader().setCid("20190529 11:38");
            new MQ(queue, msg).put();

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    
    
   private static void putMsgtoQueue(String msgText) {
        try {
            //Queue queue = getQueueSharesProd();
            Queue queue = getQueueSharesProd();

            //-- Put
            Message msg = new Message();
            msg.setMessage(msgText);
            //msg.setHeader(new Header());
            //msg.getHeader().setMsdType("");
            //msg.getHeader().setCid("20210501 01:00");
            new MQ(queue, msg).put();

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    
    public static String getStringCurrentDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }
    
    public static String getStringCurrentDateTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW_DATETIME);
        return sdf.format(cal.getTime());
    }
    
    
    public static String getStringCurrentTimeStamp() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_TIMESTAMP);
        return sdf.format(cal.getTime());
    }
    
    public static String getStringCurrentTimeStampNoSpaces() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW_DATETIME2);
        return sdf.format(cal.getTime());
    }
    
    public static void getAPISMessagesfromAtrium(){
        String fileName = "APIS-ATRIUM-Messages_"+ getStringCurrentDate() + ".txt"; 
        try{            
            List<Message> lstMsg; //= new ArrayList<Message>();
            //Queue queue = getQueueConectionTEST();
            Queue queue = getQueueConectionPROD();
            
            //Get
            //lstMsg = new ArrayList<Message>();
            lstMsg = new MQ(queue).get();

            Message data; //= null;
            if (lstMsg.size() > 0) {
                System.out.println(GetMQMessages.getStringCurrentTimeStamp() +  " Number of messages: " + lstMsg.size());
                writeTxt(fileName, "Number of messages: " + lstMsg.size() + "\n");
                writeTxt(fileName, "START >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
                writeTxt(fileName, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + "\n");
                
                for (int i = 0; i < lstMsg.size(); i++) {
                    //data = new Message();
                    data = lstMsg.get(i);

                    System.out.println("{" + i + "}");
                    if (data.getHeader() != null) {
                        writeTxt(fileName, "_TimeStamp: " + getStringCurrentTimeStamp() + "\n");
                    }
                    
                     if (isFCM(data.getMessage())) {
                         writeTxt(fileName, "Type: FCM" + "\n");
                     }
                     
                     if (isMCL(data.getMessage())) {
                         writeTxt(fileName, "Type: MLC" + "\n");
                     }
                     
                     writeTxt(fileName, "Message: \n" + printTextWithLines(data.getMessage()) + "\n");
                     try {
                        processMessage(data.getMessage(),fileName);
                        writeTxt(fileName, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + "\n");
                    }catch (Exception ex1){
                         writeTxt(fileName,"ERROR: Error processing Message from ATRIUM. " +  ex1.getMessage() + "\n");
                    }
                }
            }else{
                
                System.out.println(GetMQMessages.getStringCurrentTimeStamp() + " END - NO MESSAGES in QUEUE. \n");
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
                //writeTxt(fileName,"END - NO MESSAGES in QUEUE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    public static void processMessage(String msgText, String fileNameLog){
        
        if (isFCM(msgText)) {
            sendStandardFCMMessage(msgText);
            sendINMFCMMessage(msgText);
            
        } else {
            System.out.println(GetMQMessages.getStringCurrentTimeStamp() +" Is a MCL.");
            //ISMCL
            
            if (!PurgeQueue){
                sendMail(msgText.replaceAll("'", "'\n"), "MCL ATRIUM", "gam-fcm@aeromexico.com","");
            }
            
            String fechaActual = getStringCurrentDateTime();
            
            
            try{
            if (!PurgeQueue){
                writeTxt("C://Atrium//Apps//apisGetMessages//GetMQMessages//tempFiles//"+"MCL_"+fechaActual+".batap", printTextWithLines(msgText).toString());
                sendFile("MCL_"+fechaActual+".batap");
            }    
                
            }catch (Exception ex){
                System.out.println(GetMQMessages.getStringCurrentTimeStamp() + " " + ex.getMessage());
            }
        }
    }
    
    public static void sendStandardFCMMessage(String msgText){
        
            String subjectMessage = getSubjectFromFCMMesaage(msgText);
            
            String fileNameMessage = getFileNameFCMMesaage(msgText);
            
            System.out.println(GetMQMessages.getStringCurrentTimeStamp() + " Is a FCM message.");
            
            if (!PurgeQueue){
                sendMail(printTextWithLines(msgText).toString(), subjectMessage, "gam-fcm@aeromexico.com","");
                sendMailToCanadaFCM(printTextWithLines(msgText).toString(),subjectMessage);
            } 
            
            //send message to the Queue. 
            //putMsgtoQueue(printTextWithLines(msgText).toString());
            
            try{
                if (!PurgeQueue){
                    writeTxt("C://Atrium//Apps//apisGetMessages//GetMQMessages//tempFiles//"+ fileNameMessage +".batap", printTextWithLines(msgText).toString());
                    sendFile(fileNameMessage+".batap");
                }
              
            }catch (Exception ex){
                System.out.println(GetMQMessages.getStringCurrentTimeStamp() + " " + ex.getMessage());
                
            }
    }
    
    public static void sendINMFCMMessage(String msgText) {
        //INM reqiuere que les mandemos los vuelos de 5D
        //Se va  a filtrar por rangos de vuelos
        msgText = cleanMessageforINM(msgText);
        
        String fileNameMessage = getFileNameFCMMesaage(msgText);
        System.out.println(GetMQMessages.getStringCurrentTimeStamp() + " Is a FCM message.");

        try {
            if (!PurgeQueue) {
                writeTxt("C://Atrium//Apps//apisGetMessages//GetMQMessages//tempFiles//" + fileNameMessage + ".batap", printTextWithLines(msgText).toString());
                sendFile(fileNameMessage + ".batap");
            }
        } catch (Exception ex) {
            System.out.println(GetMQMessages.getStringCurrentTimeStamp() + " " + ex.getMessage());
        }
    }
    

    public static void sendMailToCanadaFCM(String msgText, String subject){
        if (msgText.contains("api-ipv@cbsa-asfc.gc.ca")) {
            //Mail to Canada
            sendMail(msgText.replace("QU api-ipv@cbsa-asfc.gc.ca", ""), subject, "amjefeturnoapis@aeromexico.com","api-ipv@cbsa-asfc.gc.ca");
            sendMail(msgText.replace("QU api-ipv@cbsa-asfc.gc.ca", ""), subject, "amjefeturnoapis@aeromexico.com","");
        }
    }
 
     public static void sendMail (String msgText, String subjectMail, String emailSender, String toMail){
        try {
    		Server smtp = new Server();
			smtp.setHost("172.18.60.227");
			smtp.setPort(25);
			smtp.setUser(emailSender);
			smtp.setPassword("");
    	
            //Mail oMail = new Mail(smtp, null);
            Mail oMail = new Mail(null, null);
            oMail.setTransfer(Mail.RELAY);
            oMail.setContent(Mail.TEXT);
            if (toMail.equalsIgnoreCase("")){
                oMail.setTO("fvazquez@aeromexico.com,jmendozam@aeromexico.com,pmesino@aeromexico.com,amfirmasaicm@aeromexico.com,pereyes@aeromexico.com,jordonez@aeromexico.com,cgomezm@aeromexico.com,dmontiel@aeromexico.com,balcantara@aeromexico.com,amcenlacetrip@aeromexico.com,ldel@aeromexico.com,amctripulaciones2@aeromexico.com,amjefeturnoapis@aeromexico.com,mzavala@aeromexico.com");
            }else{
                oMail.setTO(toMail);
            }
            //oMail.setCC("");
            oMail.setSubject(subjectMail);
            oMail.setMessage(msgText);
            oMail.setAttempts(1);

            //List<Attachment> lst = new ArrayList<Attachment>();
            //Attachment atch = new Attachment();
            //lst.add(atch);            
            //oMail.setAttachments(lst);
            
            new Email(oMail).send();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
     }
     
    public static boolean isFCM(String msgQueue) {
        boolean response = true;
        if (msgQueue.contains("MCL")) {
            response = false;
        }
        return response;
    }

    public static boolean isMCL(String msgQueue) {
        boolean response = false;
        if (msgQueue.contains("MCL")) {
            response = true;
        }
        return response;
    }
   
    public static String getSubjectFromFCMMesaage(String msgText) {
        String flightNumber = null;
        String origin = null;
        String destination = null;
        String departureDate = null;
        String subjectMessage = "";
        try {

            Pattern regex = Pattern.compile("TDT\\+20\\+(.*?)\\+\\+\\+", Pattern.DOTALL);
            Matcher regexMatcher = regex.matcher(msgText);
            if (regexMatcher.find()) {
                flightNumber = regexMatcher.group(1);
            }

            Pattern regex2 = Pattern.compile("LOC\\+125\\+(.*?)DTM", Pattern.DOTALL);
            Matcher regexMatcher2 = regex2.matcher(msgText);
            if (regexMatcher2.find()) {
                origin = regexMatcher2.group(1).replace("'", "");;

            }

            Pattern regex3 = Pattern.compile("LOC\\+87\\+(.*?)DTM", Pattern.DOTALL);
            Matcher regexMatcher3 = regex3.matcher(msgText);
            if (regexMatcher3.find()) {
                destination = regexMatcher3.group(1).replace("'", "");

            }

            Pattern regex4 = Pattern.compile("DTM\\+189:(.*?):201", Pattern.DOTALL);
            Matcher regexMatcher4 = regex4.matcher(msgText);
            if (regexMatcher4.find()) {
                departureDate = regexMatcher4.group(1);

            }
            
            //Si es Brazil buscamos de otra manera el nuemro de vuelo 
            if (origin.equalsIgnoreCase("GRU") || destination.equalsIgnoreCase("GRU") || origin.equalsIgnoreCase("SCL") || destination.equalsIgnoreCase("SCL")) {
                Pattern regex5 = Pattern.compile("TDT\\+20\\+(.*?)LOC", Pattern.DOTALL);
                Matcher regexMatcher5 = regex5.matcher(msgText);
                if (regexMatcher5.find()) {
                    flightNumber = regexMatcher5.group(1).replace("'", "");
                }
            }
            
            subjectMessage = "FCM-"+flightNumber + "-" + origin + "-" + destination + "-" + departureDate;
            System.out.println("APIS :" + subjectMessage);
        } catch (PatternSyntaxException ex) {
            System.out.println(ex.getMessage());
            // Syntax error in the regular expression
        }

        return subjectMessage;
    }
    
    public static String cleanMessageforINM(String msgText) {
        String flightNumber = null;
        Integer flighNumberI;

        try {

            String lines[] = msgText.split("\\r?\\n");
            StringBuilder sb = new StringBuilder();
            sb.append("QU MEXIMXH");
            sb.append("\r\n");
            sb.append(lines[1]);
            sb.append("\r\n");
            sb.append(lines[2]);
            sb.append("\r\n");
            sb.append(lines[3]);

            msgText = sb.toString();

            Pattern regex = Pattern.compile("TDT\\+20\\+AM(.*?)\\+\\+\\+", Pattern.DOTALL);
            Matcher regexMatcher = regex.matcher(msgText);
            if (regexMatcher.find()) {
                flightNumber = regexMatcher.group(1);
                System.out.println("Flight Number :" + flightNumber);
                flighNumberI = Integer.parseInt(flightNumber);
                if (flighNumberI >= 2000 && flighNumberI <= 2899) {
                    System.out.println("The flight is Operated by Aeromexico Connect");
                    msgText = msgText.replace("TDT+20+AM" + flightNumber + "+++AM", "TDT+20+5D" + flightNumber + "+++5D");
                }
            }
        } catch (PatternSyntaxException ex) {
            System.out.println(ex.getMessage());
            // Syntax error in the regular expression
        } catch (NumberFormatException ex1) {
            System.out.println(ex1.getMessage());
        }

        return msgText;

    }

    public static boolean isICNStation(String psMessage){
        return psMessage.contains("SELTSXA"); 
    }
    
    public static String cleanLOC180Section(String psMessage){
        
        String newValueofLOC = "";
        //System.out.println("Value Located: " + psMessage);
        String[] splitValues = psMessage.split("\\+");
        if (splitValues.length > 3) {
            newValueofLOC = splitValues[0] + "+" + 
                            splitValues[1] + "+" + 
                            splitValues[2] + "+" +
                            splitValues[3];
            //System.out.println("New Value: " + newValueofLOC);
        }
        return newValueofLOC;
    }
    
    public static String getFileNameFCMMesaage(String msgText) {
        String flightNumber = null;
        String origin = null;
        String destination = null;
        String departureDate = null;
        String subjectMessage = "";
        try {

            Pattern regex = Pattern.compile("TDT\\+20\\+(.*?)\\+\\+\\+", Pattern.DOTALL);
            Matcher regexMatcher = regex.matcher(msgText);
            if (regexMatcher.find()) {
                flightNumber = regexMatcher.group(1);
            }

            Pattern regex2 = Pattern.compile("LOC\\+125\\+(.*?)DTM", Pattern.DOTALL);
            Matcher regexMatcher2 = regex2.matcher(msgText);
            if (regexMatcher2.find()) {
                origin = regexMatcher2.group(1).replace("'", "");;

            }

            Pattern regex3 = Pattern.compile("LOC\\+87\\+(.*?)DTM", Pattern.DOTALL);
            Matcher regexMatcher3 = regex3.matcher(msgText);
            if (regexMatcher3.find()) {
                destination = regexMatcher3.group(1).replace("'", "");

            }

            Pattern regex4 = Pattern.compile("DTM\\+189:(.*?):201", Pattern.DOTALL);
            Matcher regexMatcher4 = regex4.matcher(msgText);
            if (regexMatcher4.find()) {
                departureDate = regexMatcher4.group(1);

            }
            
            //Si es Brazil buscamos de otra manera el nuemro de vuelo 
            if (origin.equalsIgnoreCase("GRU") || destination.equalsIgnoreCase("GRU") || origin.equalsIgnoreCase("SCL") || destination.equalsIgnoreCase("SCL")) {
                Pattern regex5 = Pattern.compile("TDT\\+20\\+(.*?)LOC", Pattern.DOTALL);
                Matcher regexMatcher5 = regex5.matcher(msgText);
                if (regexMatcher5.find()) {
                    flightNumber = regexMatcher5.group(1).replace("'", "");
                }
            }
            
            subjectMessage = "FCM-"+flightNumber + "-" + origin + "-" + destination + "-" + departureDate + "-" + getStringCurrentTimeStampNoSpaces();
            System.out.println("APIS :" + subjectMessage);
        } catch (PatternSyntaxException ex) {
            System.out.println(ex.getMessage());
            // Syntax error in the regular expression
        }

        return subjectMessage;
    }
    
    public static StringBuilder printTextWithLines(String msgText){
        String lines[] = msgText.split("\\r?\\n");
        
        StringBuilder sb = new StringBuilder();
        String []msglines;
        
        sb.append(lines[0]);
        //sb.append(" MEXIMXH"); // We appended the INM sita address in order to workarround the missing address in ATRIUM
        sb.append("\r\n");
        
        sb.append(lines[1]);
        sb.append("\r\n");
        
        sb.append(lines[2]);
        sb.append("\r\n");
        
        msglines = lines[3].split("'");
        if (isICNStation(msgText)) {
            for (int i = 0; i < msglines.length; i++) {
                if (msglines[i].contains("LOC+180")) {
                    sb.append(cleanLOC180Section(msglines[i]));
                } else {
                    sb.append(msglines[i]);
                }
                sb.append("'");
                sb.append("\r\n");
            }
        } else {
            for (int i = 0; i < msglines.length; i++) {
                sb.append(msglines[i]);
                sb.append("'");
                sb.append("\r\n");
            }
        }
        
        return sb;
    }
    
    public static void sendFile(String fileName){
        try{
          
        Ftp sftp = new Ftp();
        System.out.println("Enviando archivo " + fileName +" SFTP a SBS");
        sftp.setHost("172.18.60.121");
        sftp.setPort(22);
        sftp.setUser("mqm");
        sftp.setPwd("Cr0n3s7Ap1s");
        //           Cr0n3s7Ap1s
        //sftp.setKey("");

        //-- Exec
        //sftp.setCommand("mv test.txt test.xml && ls -ltr");
        //new Sftp(sftp).exec();
        //-- Upload
        sftp.setPath("/aero1/aero/produccion/apis/");

        List<File> lstFile = new ArrayList<File>();
        File file = new File();
        file.setPath("C://Atrium//Apps//apisGetMessages//GetMQMessages//tempFiles//");
        file.setName(fileName);
        lstFile.add(file);
        sftp.setFiles(lstFile);
        new Sftp(sftp).upload();
        
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    public static void Testscenario() {
        String messageTest = getMessageFCMTEST();
        
        sendMailToCanadaFCMTEST(messageTest,"FCM-AM636-MEX-YUL-2109271525-TEST");
        
        /*
        System.out.println("Starting Test Scenario.");
        System.out.println(getStringCurrentDateTime());
        
        String Message_test1 = " QU SELTSXA MEXCEAM MEXMSAM MEXOXAM\n"
                + ".MEXEH1Y AM/200055\n" + "\n"
                + "UNA:+.? 'UNB+UNOA:4+CEO-AM:ZZ+KIS:ZZ+210620:0255+AM007542++APIS'UNG+PAXLST+CEO-AM:ZZ+KIS:ZZ+210620:0255+AM00754210+UN+D:05B'UNH+AM00754211+PAXLST:D:05B:UN:IATA+AM91 20 ICN01'BGM+250'RFF+TN:AM91 20 ICN:::1'NAD+MS+++CONTROL DE VUELOS'COM+528182211801:TE+528182211839:FX'TDT+20+AM091+++AM'LOC+125+ICN'DTM+189:2106201225:201'LOC+87+MEX'DTM+232:2106201210:201'NAD+FM+++TORRALBA AGUERIA:RAMON'ATT+2++M'DTM+329:621002'LOC+174+ESP'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICODF+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G34627906'DTM+36:220528'LOC+91+MEX'DOC+L:110:111+200002991'DTM+36:240404'LOC+91+MEX'NAD+FM+++YANEZ MALDONADO:JUAN JOSE ENRIQUE'ATT+2++M'DTM+329:620608'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO DF+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G22635808'DTM+36:261010'LOC+91+MEX'DOC+L:110:111+200105276'DTM+36:210701'LOC+91+MEX'NAD+FM+++ALVAREZ TORRES:MIGUEL'ATT+2++M'DTM+329:691101'LOC+174+USA'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO DF+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G31091816'DTM+36:280829'LOC+91+MEX'DOC+L:110:111+200101790'DTM+36:240505'LOC+91+MEX'NAD+FM+++RAYA CASILLAS:WILHELM VON'ATT+2++M'DTM+329:680513'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO DF+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G28747944'DTM+36:240314'LOC+91+MEX'DOC+L:110:111+200300469'DTM+36:210917'LOC+91+MEX'NAD+FM+++CRUZ MACIEL:JUAN'ATT+2++M'DTM+329:760923'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G29452964'DTM+36:280508'LOC+91+MEX'DOC+L:110:111+201116410'DTM+36:211017'LOC+91+MEX'NAD+FM+++GASCA ROSILLO:LUIS MIGUEL'ATT+2++M'DTM+329:830208'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G40377913'DTM+36:310219'LOC+91+MEX'DOC+L:110:111+201217752'DTM+36:220125'LOC+91+MEX'NAD+FM+++LOPEZ NAVARRO:GABRIEL'ATT+2++M'DTM+329:790727'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO+:::LEON'NAT+2+MEX'DOC+P:110:111+G36412995'DTM+36:291031'LOC+91+MEX'DOC+L:110:111+200200198'DTM+36:210929'LOC+91+MEX'NAD+FM+++ANAYA ROMO:MANUEL ALEJANDRO'ATT+2++M'DTM+329:820415'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G29439930'DTM+36:240502'LOC+91+MEX'DOC+L:110:111+200202426'DTM+36:220731'LOC+91+MEX'NAD+FM+++BRIONES MORENO:SUSANA'ATT+2++F'DTM+329:681128'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::DISTRITO FEDERAL+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G14616622'DTM+36:240605'LOC+91+MEX'NAD+FM+++FRANCO GOMEZ:JULIA DEL CARMEN'ATT+2++F'DTM+329:690226'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::DISTRITO FEDERAL+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G19141089'DTM+36:260207'LOC+91+MEX'NAD+FM+++HERNANDEZ LARRINUA:BEATRIZ'ATT+2++F'DTM+329:700625'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO DF+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G34052628'DTM+36:290415'LOC+91+MEX'NAD+FM+++DOMINGUEZ CONTRERAS:ADA PATRICIA'ATT+2++F'DTM+329:670821'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO DF+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G31760977'DTM+36:241019'LOC+91+MEX'NAD+FM+++LOAIZA GUTIERREZ:MONICA'ATT+2++F'DTM+329:710413'LOC+174+MEX'LOC+178+ICN'LOC+179+MEX'LOC+180+MEX+:::MEXICO DF+:::DISTRITO FEDERAL'NAT+2+MEX'DOC+P:110:111+G28824881'DTM+36:280316'LOC+91+MEX'CNT+41:13'UNT+179+AM00754211'UNE+1+AM00754210'UNZ+1+AM007542'";

        String Message_test ="QU MEXCEAM MEXMSAM MEXJUAM DCAUCCR MEXIMXH\n"
                +".MEXEH1Y AM/231246\n" + "\n"
                +"UNA:+.? 'UNB+UNOA:4+CEO-AM:ZZ+USCSAPIS:ZZ+210823:1446+AM016913++APIS'UNG+PAXLST+CEO-AM:ZZ+USCSAPIS:ZZ+210823:1446+AM01691310+UN+D:05B'UNH+AM01691311+PAXLST:D:05B:UN:IATA+AM2682 23 MEX01'BGM+250+C'RFF+TN:AM2682 23 MEX:::1'NAD+MS+++CONTROL DE VUELOS'COM+528182211801:TE+528182211839:FX'TDT+20+AM0682+++AM'LOC+125+MEX'DTM+189:2108230915:201'LOC+87+DFW'DTM+232:2108231200:201'NAD+FM+++NAVARRO BOLIO:RODRIGO ALEJANDRO+MAR DE LA FERTILIDAD 33 CIUDAD BRIS+NAUCALPAN DE JUAREZ++53280+MEX'ATT+2++M'DTM+329:730726'LOC+22+DFW'LOC+174+MEX'LOC+178+MEX'LOC+179+DFW'LOC+180+MEX+:::NAUCALPAN DE JUAREZ+:::NAUCALPAN DE JUAREZ'EMP+1+CR1:110:111'NAT+2+MEX'DOC+P:110:111+G35805653'DTM+36:290816'LOC+91+MEX'DOC+L:110:111+200109922'DTM+36:220127'LOC+91+MEX'NAD+FM+++GAYTAN GUTIERREZ:DAVID+ESPERANZA 11 ESPERANZA+EDOMEX++00000+MEX'ATT+2++M'DTM+329:850429'LOC+22+DFW'LOC+174+MEX'LOC+178+MEX'LOC+179+DFW'LOC+180+MEX+:::TOLUCA+:::TOLUCA'EMP+1+CR1:110:111'NAT+2+MEX'DOC+P:110:111+G40462767'DTM+36:270317'LOC+91+MEX'DOC+L:110:111+201532159'DTM+36:220313'LOC+91+MEX'NAD+FM+++FLORES RAMOS:JOANA PATRICIA+SANTA CATARINA 353 LOMAS DEL PONIEN+MONTERREY++00000+MEX'ATT+2++F'DTM+329:851227'LOC+22+DFW'LOC+174+MEX'LOC+178+MEX'LOC+179+DFW'LOC+180+MEX+:::NUEVO LEON+:::MONTERREY'EMP+1+CR2:110:111'NAT+2+MEX'DOC+P:110:111+G39990599'DTM+36:261209'LOC+91+MEX'NAD+FM+++MARTINEZ DIAZ:AZUCENA+CANELA 484 B501+CIUDAD DE MEXICO++08400+MEX'ATT+2++F'DTM+329:810623'LOC+22+DFW'LOC+174+MEX'LOC+178+MEX'LOC+179+DFW'LOC+180+MEX+:::NUEVO LEON+:::MONTERREY'EMP+1+CR2:110:111'NAT+2+MEX'DOC+P:110:111+G39882072'DTM+36:301204'LOC+91+MEX'NAD+FM+++COLORADO SANCHEZ:NALLELY+CLL SELVA 16+MEXICO++54740+MEX'ATT+2++F'DTM+329:970225'LOC+22+DFW'LOC+174+MEX'LOC+178+MEX'LOC+179+DFW'LOC+180+MEX+:::MEXICO+:::DISTRITO FEDERAL'EMP+1+CR2:110:111'NAT+2+MEX'DOC+P:110:111+G23027518'DTM+36:221109'LOC+91+MEX'CNT+41:5'UNT+83+AM01691311'UNE+1+AM01691310'UNZ+1+AM016913'";
        if (isMCL(Message_test)) {
            System.out.println("Is MCL");
        }
        if (isFCM(Message_test)) {
            System.out.println("Is FCM");
            System.out.println("Message: \n" + Message_test);
            if(isICNStation(Message_test)){
                System.out.println("Is Flight ICN");
                //System.out.println("Cleaned Message: \n"+ printTextWithLines(Message_test));
            }
            else{
                System.out.println("Is different than ICN");
                //System.out.println("Cleaned Message: \n"+ printTextWithLines(Message_test));
            }
            
            Message_test = cleanMessageforINM(Message_test);
            System.out.println("Cleaned Message INM: \n"+ printTextWithLines(Message_test));
        }
*/
    }
    
    public static void sendMailToCanadaFCMTEST(String msgText, String subject){
            //Mail to Canada
            sendMailTEST(msgText, subject, "gam-fcm@aeromexico.com","");
        
    }
    
    public static void sendMailTEST (String msgText, String subjectMail, String emailSender, String toMail){
        try {
    		Server smtp = new Server();
			smtp.setHost("172.18.60.227");
			smtp.setPort(25);
			smtp.setUser(emailSender);
			smtp.setPassword("");
    	
            //Mail oMail = new Mail(smtp, null);
            Mail oMail = new Mail(null, null);
            oMail.setTransfer(Mail.RELAY);
            oMail.setContent(Mail.TEXT);
            if (toMail.equalsIgnoreCase("")){
                oMail.setTO("api-ipv-certification@cbsa-asfc.gc.ca,pereyes@aeromexico.com");
            }else{
                oMail.setTO(toMail);
            }
            //oMail.setCC("");
            oMail.setSubject(subjectMail);
            oMail.setMessage(msgText);
            oMail.setAttempts(1);

            //List<Attachment> lst = new ArrayList<Attachment>();
            //Attachment atch = new Attachment();
            //lst.add(atch);            
            //oMail.setAttachments(lst);
            
            new Email(oMail).send();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
     }
     
    
    public static void GetMessagesFromQueue(){
        try{            
            List<Message> lstMsg;
            Queue queue = getQueueConectionTEST();
            //Queue queue = getQueueConectionPROD();
            
            //Get
            //lstMsg = new ArrayList<Message>();
            lstMsg = new MQ(queue).get();

            Message data = null;
            if (lstMsg.size() > 0) {
                System.out.println(GetMQMessages.getStringCurrentTimeStamp() +  " Number of messages: " + lstMsg.size());
                for (int i = 0; i < lstMsg.size(); i++) {
                    //data = new Message();
                    data = lstMsg.get(i);

                    System.out.println("{" + i + "}");
                    if (data.getHeader() != null) {
                         System.out.println( "_TimeStamp: " + getStringCurrentTimeStamp() + "\n");
                    } 
                      System.out.println("Message: \n" + printTextWithLines(data.getMessage()) + "\n");
                }
            }else{
                
                System.out.println(GetMQMessages.getStringCurrentTimeStamp() + " END - NO MESSAGES in QUEUE. \n");
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
                //writeTxt(fileName,"END - NO MESSAGES in QUEUE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    
    public static String getMessageFCMTEST() {
        //Mensaje para probar canada
        return "UNA:+.? '\n"
                + "UNB+UNOA:4+CEO-AM:ZZ+CCRAAPIS:ZZ+210927:1547+AM022048++APIS'\n"
                + "UNG+PAXLST+CEO-AM:ZZ+CCRAAPIS:ZZ+210927:1547+AM02204810+UN+D:02B'\n"
                + "UNH+AM02204811+PAXLST:D:02B:UN:IATA+AM636 27 MEX01'\n"
                + "BGM+250'\n"
                + "NAD+MS+++CONTROL DE VUELOS'\n"
                + "COM+528182211801:TE+528182211839:FX'\n"
                + "TDT+20+AM636'\n"
                + "LOC+125+MEX'\n"
                + "DTM+189:2109271525:201'\n"
                + "LOC+87+YUL'\n"
                + "DTM+232:2109272145:201'\n"
                + "NAD+FM+++HAROLD:CLARKE+++++'\n"
                + "ATT+2++M'\n"
                + "DTM+329:850326'\n"
                + "LOC+174+CAN'\n"
                + "LOC+178+YVR'\n"
                + "LOC+179+MEX'\n"
                + "LOC+180+MEX+:::MEXICO DF+:::DISTRITO FEDERAL'\n"
                + "EMP+1+CR1:110:111'\n"
                + "NAT+2+CAN'\n"
                + "DOC+C:110:111+KX123600'\n"
                + "DTM+36:260615'\n"
                + "LOC+91+CAN'\n"
                + "DOC+L:110:111+201743622'\n"
                + "DTM+36:210701'\n"
                + "LOC+91+CAN'\n"
                + "NAD+FM+++SIMPSON:LESLIE+++++'\n"
                + "ATT+2++F'\n"
                + "DTM+329:760315'\n"
                + "LOC+174+USA'\n"
                + "LOC+178+YVR'\n"
                + "LOC+179+MEX'\n"
                + "LOC+180+MEX+:::MEXICO DF+:::DISTRITO FEDERAL'\n"
                + "EMP+1+CR2:110:111'\n"
                + "NAT+2+USA'\n"
                + "DOC+P:110:111+96B9ST1A'\n"
                + "DTM+36:260614'\n"
                + "LOC+91+USA'\n"
                + "NAD+FM+++HUDSEN:ARNOLD+++++'\n"
                + "ATT+2++F'\n"
                + "DTM+329:910827'\n"
                + "LOC+174+CAN'\n"
                + "LOC+178+YVR'\n"
                + "LOC+179+MEX'\n"
                + "LOC+180+MEX+:::DISTRITO FEDERAL+:::DISTRITO FEDERAL'\n"
                + "EMP+1+CR2:110:111'\n"
                + "NAT+2+CAN'\n"
                + "DOC+F:110:111+DH569874'\n"
                + "CNT+41:3'\n"
                + "UNT+77+AM02204811'\n"
                + "UNE+1+AM02204810'\n"
                + "UNZ+1+AM022048'\n"
                + "";

    }
}
